#ifndef GENERIC_H
#define GENERIC_H

#define assert(e) ((e) ? (void)0 : __assert(__func__, __FILE__, __LINE__, #e))

extern void __assert(const char *__func, const char *__file, int __lineno,
                     const char *__sexp);

typedef void (*ButtonStateFunc)();
typedef void (*TimerFunc)();
typedef void (*ButtonStateTimerFunc)(unsigned long, unsigned long);
typedef void (*ButtonSameStateFunc)(bool, unsigned long, unsigned long,
                                    unsigned long);

class TickTimer {
 public:
  TickTimer(TimerFunc timerFunc);
  void Start(unsigned long delay, unsigned long currTime);
  void Progress(unsigned long currTime);

 private:
  TimerFunc _timerFunc;
  unsigned long _delay;
  unsigned long _lastTime;
};

class ButtonState {
 public:
  ButtonState(ButtonStateFunc PressStateFunc,
              ButtonStateTimerFunc PressStateTimerFunc,
              ButtonStateFunc ReleaseStateFunc,
              ButtonStateTimerFunc ReleaseStateTimerFunc,
              ButtonSameStateFunc SameStateFunc);
  void Start(unsigned long currTime);
  void Progress(int pin, unsigned long currTime);

 private:
  bool _lastButtonState;
  unsigned long _lastPressStateTime;
  unsigned long _lastReleaseStateTime;
  ButtonStateFunc _PressStateFunc;
  ButtonStateTimerFunc _PressStateTimerFunc;
  ButtonStateFunc _ReleaseStateFunc;
  ButtonStateTimerFunc _ReleaseStateTimerFunc;
  ButtonSameStateFunc _SameStateFunc;
};

#endif
