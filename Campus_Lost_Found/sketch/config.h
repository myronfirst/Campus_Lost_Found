#ifndef CONFIG_H
#define CONFIG_H

extern byte MaxRetriesBeforeAssert;

extern const int ButtonPin;

extern const int PlayerIdPin0;
extern const int PlayerIdPin1;

extern const int RedPin;
extern const int GreenPin;
extern const int BluePin;
extern const int DebugPin;

extern const int RSSIThreshold;
extern const char GameNetSSID[];
extern const byte GameServerIP[4];
extern const int GameServerPort;

extern const unsigned long UploadDelay;
extern const unsigned long PairDelayMin;
extern const unsigned long PairDelayMax;

extern const unsigned long LEDPowerTimer;

extern float MinDistance;
extern unsigned long MinBlinkDelay;
extern float MaxDistance;
extern unsigned long MaxBlinkDelay;

extern unsigned long StatusDelay;
extern unsigned long GPSPollDelay;

#endif
