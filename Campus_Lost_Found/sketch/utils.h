#ifndef UTILS_H
#define UTILS_H

#include <WiFiNINA.h>

bool IsPlayerSSID(const char *ssid);
int GetPlayerNumber(const char *ssid);

bool ConnectToGameNetAndServer(WiFiClient client);
void MakeHTTPRequest(const char *requestLine, WiFiClient client);

bool IsButtonPressed(int pin);

void EnableLED(byte r, byte g, byte b, unsigned long delayTime);
void DisableLED(unsigned long delayTime);
void BlinkLED(byte r, byte g, byte b, unsigned long onDelay,
              unsigned long offDelay, unsigned seconds);

void Error(const char *msg);

#endif
