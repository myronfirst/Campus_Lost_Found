#include "gps.h"

#define GPGGA_BUF_SIZE 128  // 79 is the max
#define GPGGA_HEADER_LENGTH 6
#define GPGGA_READ_MAX_ATTEMPTS 10

char GPGGABuf[GPGGA_BUF_SIZE];
int GPGGABufIndex = 0;
const char* GPGGADefault =
    "$GPGGA,184353.07,1929.045,S,02410.506,E,1,04,2.6,100.00,M,-33.9,M,,0000*"
    "6D";
char lineBuffer[GPGGA_BUF_SIZE];

void ResetGPGGABuf() {
  ClearGPGGABuf();
  strcpy(GPGGABuf, GPGGADefault);
  GPGGABufIndex = 0;
}
void ClearGPGGABuf() { memset(GPGGABuf, 0, GPGGA_BUF_SIZE); }

void InitGPGGA() {
  Serial1.begin(9600);
  ResetGPGGABuf();
}

void ReadLine() {
  memset(lineBuffer, 0, GPGGA_BUF_SIZE);
  int i = 0;
  while (true) {
    while (Serial1.available() > 0) {
      assert((i < GPGGA_BUF_SIZE) && "GPGGA Buffer Overflow");
      lineBuffer[i] = Serial1.read();
      if (lineBuffer[i] == '$') {
        lineBuffer[i] = '\0';
        // Serial.print(lineBuffer);
        return;
      }
      i++;
    }
  }
  assert(false && "Outside while(true) loop");
}

bool FoundGPGGAHeader() {
  int val;
  //   val = strncmp(lineBuffer, "$GPGGA", GPGGA_HEADER_LENGTH);
  val = strncmp(lineBuffer, "GPGGA", GPGGA_HEADER_LENGTH - 1);
  return (val == 0);
}

void UpdateGPGGABuf() {
  //   Serial.println("Update GPS begin");
  for (int i = 0; i < GPGGA_READ_MAX_ATTEMPTS; ++i) {
    // Serial.println(i);
    ReadLine();
    if (FoundGPGGAHeader()) {
      String str = String('$');
      str.concat(String(lineBuffer));
      str.trim();
      strcpy(GPGGABuf, str.c_str());
      break;
    }
  }
  Serial.print(GPGGABuf);
  //   Serial.println("Update GPS end");
}

const char* GetGPSBuf() { return GPGGABuf; }

/*
void setup() {
  Serial.begin(9600);
  InitGPGGA();
}

void loop() {
  UpdateGPGGABuf();
  // Serial.print(GPGGABuf);
}
*/
