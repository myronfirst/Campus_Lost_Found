#include "generic.h"
// handle diagnostic informations given by assertion and abort program
// execution:
void __assert(const char *__func, const char *__file, int __lineno,
              const char *__sexp) {
  // transmit diagnostic informations through serial link.
  Serial.println(__func);
  Serial.println(__file);
  Serial.println(__lineno, DEC);
  Serial.println(__sexp);
  Serial.flush();
  // abort program execution.
  abort();
}

TickTimer::TickTimer(TimerFunc timerFunc) {
  _delay = 0;
  _timerFunc = timerFunc;
  _lastTime = 0;
}
void TickTimer::Start(unsigned long delay, unsigned long currTime) {
  _delay = delay;
  _lastTime = currTime;
}
void TickTimer::Progress(unsigned long currTime) {
  if (currTime > _lastTime && currTime - _lastTime >= _delay) {
    if (_timerFunc) _timerFunc();
    _lastTime = millis();
    // _lastTime += _delay;
  }
}

ButtonState::ButtonState(ButtonStateFunc PressStateFunc,
                         ButtonStateTimerFunc PressStateTimerFunc,
                         ButtonStateFunc ReleaseStateFunc,
                         ButtonStateTimerFunc ReleaseStateTimerFunc,
                         ButtonSameStateFunc SameStateFunc) {
  _lastButtonState = false;
  _lastPressStateTime = 0;
  _lastReleaseStateTime = 0;
  _PressStateFunc = PressStateFunc;
  _PressStateTimerFunc = PressStateTimerFunc;
  _ReleaseStateFunc = ReleaseStateFunc;
  _ReleaseStateTimerFunc = ReleaseStateTimerFunc;
  _SameStateFunc = SameStateFunc;
}
void ButtonState::Start(unsigned long currTime) {
  _lastPressStateTime = currTime;
  _lastReleaseStateTime = currTime;
}
void ButtonState::Progress(int pin, unsigned long currTime) {
  bool isPressed = IsButtonPressed(pin);
  if (isPressed == _lastButtonState) {
    if (_SameStateFunc)
      _SameStateFunc(isPressed, _lastPressStateTime, _lastReleaseStateTime,
                     currTime);
    return;
  }
  _lastButtonState = isPressed;
  if (isPressed) {
    _lastPressStateTime = currTime;
    if (_PressStateFunc) _PressStateFunc();
    if (_PressStateTimerFunc)
      _PressStateTimerFunc(_lastReleaseStateTime, currTime);
  } else {
    _lastReleaseStateTime = currTime;
    if (_ReleaseStateFunc) _ReleaseStateFunc();
    if (_ReleaseStateTimerFunc)
      _ReleaseStateTimerFunc(_lastPressStateTime, currTime);
  }
}
