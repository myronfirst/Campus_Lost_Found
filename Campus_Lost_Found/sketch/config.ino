#include "config.h"

byte MaxRetriesBeforeAssert = 3;

const int ButtonPin = 6;

const int PlayerIdPin0 = 8;
const int PlayerIdPin1 = 7;

const int RedPin = 12;
const int GreenPin = 11;
const int BluePin = 10;
const int DebugPin = LED_BUILTIN;

const int RSSIThreshold = -40;

const char GameNetSSID[] = "myronfirst_wifi";
const byte GameServerIP[4] = {192, 168, 43, 177};  // my laptop
// const char GameNetSSID[] = "LostAndFound";
// const byte GameServerIP[4] = {192, 168, 0, 101};  // viktor laptop
// const byte GameServerIP[4] = {192, 168, 0, 106};  // chenxu laptop
const int GameServerPort = 8080;

const unsigned long UploadDelay = 10 * 1000;
const unsigned long PairDelayMin = 2 * 1000;
const unsigned long PairDelayMax = UploadDelay;

const unsigned long LEDPowerTimer = 5 * 1000;

float MinDistance = 0.0f;
unsigned long MinBlinkDelay = 0.1 * 1000;

float MaxDistance = 500.0f;
unsigned long MaxBlinkDelay = 1 * 1000;

unsigned long StatusDelay = 5 * 1000;
unsigned long GPSPollDelay = 20 * 1000;
