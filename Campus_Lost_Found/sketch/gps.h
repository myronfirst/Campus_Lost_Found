#ifndef GPS_H
#define GPS_H

void InitGPGGA();
void UpdateGPGGABuf();
const char* GetGPSBuf();

#endif
