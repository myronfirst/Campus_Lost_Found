#include <WiFiNINA.h>

#include "config.h"
#include "generic.h"
#include "gps.h"
#include "utils.h"

/** File Specific Globals **/
byte PlayerId;

WiFiClient PlayerClient;

/** File Specific Globals End **/

/** Timers **/

void StatusFunc() {
  int status = WiFi.status();
  String msg = "";
  switch (status) {
    case WL_CONNECTED:
      msg.concat("Connected to Wifi Network");
      break;
    case WL_AP_CONNECTED:
      msg.concat("Connected to AP Mode");
      break;
    case WL_AP_LISTENING:
      msg.concat("Listening in AP Mode");
      break;
    case WL_NO_MODULE:
      msg.concat("No Wifi Module");
      break;
    case WL_IDLE_STATUS:
      msg.concat("Idle");
      break;
    case WL_NO_SSID_AVAIL:
      msg.concat("No SSID Available");
      break;
    case WL_SCAN_COMPLETED:
      msg.concat("Scan Complete");
      break;
    case WL_CONNECT_FAILED:
      msg.concat("Connection Failed");
      break;
    case WL_CONNECTION_LOST:
      msg.concat("Connection Lost");
      break;
    case WL_DISCONNECTED:
      msg.concat("Disconnected from Network");
      break;
    default:
      assert(false && "Default case");
      break;
  }
  Serial.println(msg.c_str());
}

void GPSFunc() { UpdateGPGGABuf(); }

void LEDPress() { EnableLED(255, 0, 0, 0); }

void LEDRelease() { DisableLED(0); }

void PairingFunc(unsigned long lastPressTime, unsigned long currTime) {
  if (currTime <= lastPressTime) return;
  if (currTime - lastPressTime < PairDelayMin ||
      currTime - lastPressTime > PairDelayMax)
    return;

  Serial.println("Pairing begin");
  EnableLED(0, 0, 255, 0);
  // digitalWrite(LED_BUILTIN, HIGH);
  // delay(10 * 1000);
  // Pick the nearest player network
  long keyRSSI = -9999;
  int pairedPlayerNum = -1;
  bool foundKeySSID = false;
  Serial.println("Network Scan begin");
  int numSSID = WiFi.scanNetworks();
  if (numSSID == 0) {
    Error("No Networks discovered");
    return;
  }
  for (int thisNet = 0; thisNet < numSSID; thisNet++) {
    Serial.print(WiFi.SSID(thisNet));
    Serial.print(" ");
    Serial.print(WiFi.RSSI(thisNet));
    Serial.println();
    const char *ssid = WiFi.SSID(thisNet);
    if (!IsPlayerSSID(ssid)) continue;
    long rssi = WiFi.RSSI(thisNet);
    if (rssi < keyRSSI) continue;
    keyRSSI = rssi;
    pairedPlayerNum = GetPlayerNumber(ssid);
    foundKeySSID = true;
  }
  if (!foundKeySSID) {
    Error("Player Network Not Found");
    return;
  }
  if (keyRSSI < RSSIThreshold) {
    Error("Player Network Found, but signal strength is below threshold");
    return;
  }

  Serial.println("Paired with player ");
  Serial.println(pairedPlayerNum);

  if (!ConnectToGameNetAndServer(PlayerClient)) return;

  char requestLine[256] = {0};
  sprintf(requestLine, "GET /get?key=%d_%d&pos=%s HTTP/1.1", PlayerId,
          pairedPlayerNum, GetGPSBuf());
  MakeHTTPRequest(requestLine, PlayerClient);

  Serial.println("Pairing end");
  //   EnableLED(0, 255, 0, LEDPowerTimer);
  //   DisableLED(0);
}

void PairingLEDFunc(bool isPressed, unsigned long lastPressStateTime,
                    unsigned long lastReleaseStateTime,
                    unsigned long currTime) {
  if (!isPressed) return;
  if (currTime <= lastPressStateTime) return;
  if (currTime - lastPressStateTime < PairDelayMin ||
      currTime - lastPressStateTime > PairDelayMax)
    return;

  EnableLED(0, 0, 255, 0);
}

void UploadFunc(unsigned long lastPressTime, unsigned long currTime) {
  if (currTime <= lastPressTime) return;
  if (currTime - lastPressTime < UploadDelay) return;

  Serial.println("Uploading GPS begin");
  EnableLED(0, 0, 255, 0);

  if (!ConnectToGameNetAndServer(PlayerClient)) return;
  char requestLine[256] = {0};
  //   sprintf(requestLine, "GET /update?key=%d&pos=%s HTTP/1.1\r\n", PlayerId,
  //   GetGPSBuf());
  //   sprintf(requestLine, "GET /update?key=%d&pos=%s HTTP/1.1\r\n", PlayerId,
  //   GetGPSBuf());
  //   sprintf(requestLine, "GET /update?key=%d&pos=%s \r\n", PlayerId,
  //   GetGPSBuf());
  sprintf(requestLine, "GET /pub/WWW/TheProject.html HTTP/1.1");
  MakeHTTPRequest(requestLine, PlayerClient);

  Serial.println("Uploading GPS end");
  EnableLED(0, 255, 0, LEDPowerTimer);
  DisableLED(0);
}

void UploadLEDFunc(bool isPressed, unsigned long lastPressStateTime,
                   unsigned long lastReleaseStateTime, unsigned long currTime) {
  if (!isPressed) return;
  if (currTime <= lastPressStateTime) return;
  if (currTime - lastPressStateTime < UploadDelay) return;

  EnableLED(255, 0, 255, 0);
}

TickTimer StatusTimer(StatusFunc);
TickTimer GPSPollTimer(GPSFunc);
ButtonState PairingButton(LEDPress, NULL, LEDRelease, PairingFunc,
                          PairingLEDFunc);
ButtonState UploadButton(NULL, NULL, NULL, UploadFunc, UploadLEDFunc);

void StartTimers() {
  unsigned long currentTime = millis();

  //   StatusTimer.Start(StatusDelay, currentTime);
  GPSPollTimer.Start(GPSPollDelay, currentTime);
  PairingButton.Start(currentTime);
  UploadButton.Start(currentTime);
}

void ProgressTimers() {
  unsigned long currentTime = millis();
  //   StatusTimer.Progress(currentTime);
  GPSPollTimer.Progress(currentTime);
  PairingButton.Progress(ButtonPin, currentTime);
  UploadButton.Progress(ButtonPin, currentTime);
}

/** Timers End **/

void Startup() {
  // Initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ;  // wait for serial port to connect. Needed for native USB port only
  }

  assert(WiFi.status() != WL_NO_MODULE &&
         "Communication with WiFi module failed!");

  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println(fv == WIFI_FIRMWARE_LATEST_VERSION &&
                   "Please upgrade the firmware");
  }
}

void InitPlayerId() {
  pinMode(PlayerIdPin0, INPUT);
  pinMode(PlayerIdPin1, INPUT);
  int low = digitalRead(PlayerIdPin0);
  int high = digitalRead(PlayerIdPin1);
  PlayerId = high << 1 | low;
}

void InitLED() {
  pinMode(DebugPin, OUTPUT);
  pinMode(RedPin, OUTPUT);
  pinMode(GreenPin, OUTPUT);
  pinMode(BluePin, OUTPUT);
  DisableLED(0);
}

void InitButton() { pinMode(ButtonPin, INPUT_PULLUP); }

void InitGPS() { InitGPGGA(); }

// void CheckServerAnswer() {
//   while (PlayerClient.connected()) {
//     Serial.println("Receiving Server Response");
//     while (PlayerClient.available()) {
//       char c = PlayerClient.read();
//       assert(ResBufIndex < RES_BUF_SIZE && "ResBuf overflow");
//       ResBuf[ResBufIndex] = c;
//       ++ResBufIndex;
//       Serial.print(c);
//     }
//     Serial.println("Server Response Received");
//   }
//   if (!PlayerClient.connected()) PlayerClient.stop();
// }

// void CheckAnswerParsing() {
//   if (PlayerClient.connected()) return;
//   if (ResBufIndex == 0) return;

//   Serial.println("Game Server Response parsing");
//   // Get a distance number
//   String result = String(ResBuf);
//   char validChar = result.indexOf('\'') + 1;
//   String distanceStr =
//       result.substring(result.indexOf('\'') + 2, result.lastIndexOf('\''));
//   distanceStr.trim();
//   Serial.print("Response:");
//   Serial.println(distanceStr);
//   for (int i = 0; i < (int)distanceStr.length(); ++i) {
//     char c = distanceStr[i];
//     assert((isDigit(c) || c == '.') && "Invalid Distance");
//   }
//   bool valid = (validChar == '1') ? true : false;
//   if (!valid) {
//     Error("GPGGA Server Parsing Error");
//     return;
//   }
//   float distance = distanceStr.toFloat();
//   assert(distance >= 0.0f);

//   // Clean Up
//   memset(ResBuf, 0, sizeof(ResBuf));
//   ResBufIndex = 0;

//   // Blink LED according to distance
//   unsigned long blinkDelay =
//       map(distance, MinDistance, MaxDistance, MinBlinkDelay, MaxBlinkDelay);
//   blinkDelay = constrain(blinkDelay, MinBlinkDelay, MaxBlinkDelay);
//   BlinkLED(0, 255, 0, blinkDelay, blinkDelay, LEDPowerTimer);
// }

void CheckAccessPoint() {
  if (WiFi.status() != WL_AP_LISTENING && !PlayerClient.connected()) {
    PlayerClient.stop();
    WiFi.end();
    String playerSSID = "player_ssid_";
    playerSSID.concat(PlayerId);
    {
      int ret = WL_CONNECT_FAILED;
      for (byte i = 0; i < MaxRetriesBeforeAssert; ++i) {
        ret = WiFi.beginAP(playerSSID.c_str());
        if (ret == WL_AP_LISTENING) break;
      }
      assert(ret == WL_AP_LISTENING && "Switching to Access Point failed");
    }
    Serial.println("Switched to Access Point Mode");
  }
}

void setup() {
  Startup();
  InitPlayerId();
  InitLED();
  InitButton();
  InitGPS();

  StartTimers();

  //   Serial.print(digitalRead(PlayerIdPin1));
  //   Serial.print(digitalRead(PlayerIdPin0));
  //   Serial.println();

  //   EnableLED(255, 0, 0, 4000);
  //   EnableLED(0, 255, 0, 4000);
  //   EnableLED(0, 0, 255, 4000);
  //   DisableLED(0);

  Serial.print("Welcome Player ");
  Serial.println(PlayerId);

  //   UploadFunc(0, 30 * 1000);
  // PairingFunc(0, 30 * 1000);
}

void loop() {
  //   CheckServerAnswer();
  //   CheckAnswerParsing();
  CheckAccessPoint();

  ProgressTimers();

  //   Serial.println(IsButtonPressed(ButtonPin));
}
