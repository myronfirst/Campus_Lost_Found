#include <WiFiNINA.h>

#include "config.h"
#include "generic.h"
#include "utils.h"

#define PLAYER_SSID_LIST_SIZE 4
const char PlayerSSIDList[PLAYER_SSID_LIST_SIZE][20] = {
    "player_ssid_0", "player_ssid_1", "player_ssid_2", "player_ssid_3"};

#define RES_BUF_SIZE 1024
char ResBuf[RES_BUF_SIZE];
int ResBufIndex;

bool IsPlayerSSID(const char *ssid) {
  for (int i = 0; i < PLAYER_SSID_LIST_SIZE; ++i) {
    bool isEqual = String(PlayerSSIDList[i]).equals(String(ssid));
    if (isEqual) return true;
  }
  return false;
}

int GetPlayerNumber(const char *ssid) {
  for (int i = 0; i < PLAYER_SSID_LIST_SIZE; ++i) {
    bool isEqual = String(PlayerSSIDList[i]).equals(String(ssid));
    if (isEqual) return i;
  }
  assert(false && "Invalid player Index");
  return -1;
}

bool ConnectToGameNetAndServer(WiFiClient client) {
  Serial.print("Connecting to Game Net ");
  Serial.println(GameNetSSID);
  {
    int ret = WL_IDLE_STATUS;
    for (byte i = 0; i < MaxRetriesBeforeAssert; ++i) {
      ret = WiFi.begin(GameNetSSID);
      if (ret == WL_CONNECTED) break;
    }
    if (ret != WL_CONNECTED) {
      Error("Failed to connect to Game Net");
    }
  }

  Serial.print("Connecting to Game Server ");
  Serial.print(GameServerIP[0]);
  Serial.print(".");
  Serial.print(GameServerIP[1]);
  Serial.print(".");
  Serial.print(GameServerIP[2]);
  Serial.print(".");
  Serial.print(GameServerIP[3]);
  Serial.print(":");
  Serial.print(GameServerPort);
  Serial.println();

  {
    bool ret = false;
    for (byte i = 0; i < MaxRetriesBeforeAssert; ++i) {
      ret = client.connect(GameServerIP, GameServerPort);
      if (ret == true) break;
    }
    if (ret == false) {
      Error("Failed to connect to Game Server");
      return false;
    }
  }

  delay(1000);
  Serial.println("Connected to Game Server");
  return true;
}

void ReceiveAnswer() {
  Serial.println("Receiving Server Response");
  while (PlayerClient.available()) {
    char c = PlayerClient.read();
    assert(ResBufIndex < RES_BUF_SIZE && "ResBuf overflow");
    ResBuf[ResBufIndex] = c;
    ++ResBufIndex;
    Serial.print(c);
  }
  Serial.println("Server Response Received");
}

void ParseAnswer() {
  assert(PlayerClient.connected());

  Serial.println("Game Server Response parsing");
  // Get a distance number
  String result = String(ResBuf);
  char validChar = result.indexOf('\'') + 1;
  String distanceStr =
      result.substring(result.indexOf('\'') + 2, result.lastIndexOf('\''));
  distanceStr.trim();
  Serial.print("Response:");
  Serial.println(distanceStr);
  for (int i = 0; i < (int)distanceStr.length(); ++i) {
    char c = distanceStr[i];
    assert((isDigit(c) || c == '.') && "Invalid Distance");
  }
  bool valid = (validChar == '1') ? true : false;
  if (!valid) {
    Error("GPGGA Server Parsing Error");
    return;
  }
  float distance = distanceStr.toFloat();
  assert(distance >= 0.0f);

  // Clean Up
  memset(ResBuf, 0, sizeof(ResBuf));
  ResBufIndex = 0;

  // Blink LED according to distance
  unsigned long blinkDelay =
      map(distance, MinDistance, MaxDistance, MinBlinkDelay, MaxBlinkDelay);
  blinkDelay = constrain(blinkDelay, MinBlinkDelay, MaxBlinkDelay);
  BlinkLED(0, 255, 0, blinkDelay, blinkDelay, LEDPowerTimer);
}

void MakeHTTPRequest(const char *requestLine, WiFiClient client) {
  Serial.println("Making HTTP request");
  char buf[128] = {0};
  sprintf(buf,
          "Host: %d.%d.%d.%d:%d\r\n"
          "Connection: close\r\n",
          GameServerIP[0], GameServerIP[1], GameServerIP[2], GameServerIP[3],
          GameServerPort);
  delay(1000);
  client.println(requestLine);
  client.print(buf);
  //   client.println("Accept: text/html");
  client.println();

  Serial.println(requestLine);
  Serial.print(buf);

  while (client.connected() && !client.available()) {
    delay(100);
  }

  if (client.connected()) {  // Only go there if pairing
    ReceiveAnswer();
    ParseAnswer();
  }

  Serial.println("Disconnected from Game Server");
  client.stop();
}

bool IsButtonPressed(int pin) {
  return !digitalRead(pin);  // Negation because of INPUT_PULLUP
}

void WriteLED(byte r, byte g, byte b, unsigned long delayTime) {
  assert((r < 256) && (g < 256) && (b < 256));
  analogWrite(RedPin, r);
  analogWrite(GreenPin, g);
  analogWrite(BluePin, b);
  if (delayTime > 0) delay(delayTime);
}
void EnableLED(byte r, byte g, byte b, unsigned long delayTime) {
  WriteLED(r, g, b, delayTime);
}
void DisableLED(unsigned long delayTime) { WriteLED(0, 0, 0, delayTime); }
void BlinkLED(byte r, byte g, byte b, unsigned long onDelay,
              unsigned long offDelay, unsigned seconds) {
  unsigned long startTime = millis();
  unsigned long duration = seconds * 1000;
  while (millis() - startTime < duration) {
    EnableLED(r, g, b, onDelay);
    DisableLED(offDelay);
  }
}

void Error(const char *msg) {
  Serial.println(msg);
  EnableLED(255, 0, 0, LEDPowerTimer);
  DisableLED(0);
}
