#ifndef UTILS_H
#define UTILS_H

void PrintMacAddress(byte mac[]);
void PrintEncryptionType(int thisType);
void Print2Digits(byte thisByte);
void ListNetworks();
void PrintWifiStatus();

enum TimerType {
  SCAN_REFRESH = 0
};

class TickTimer {
  public:
    TickTimer(unsigned long delay, TimerType type);
    void Start(unsigned long t);
    void Progress(unsigned long currTime);

  private:
    TimerType _type;
    unsigned long _delay;
    unsigned long _lastTime;
};


#endif
