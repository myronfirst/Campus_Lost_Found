#include <Arduino.h>

#include "utils.h"

#include <SPI.h>
#include <WiFiNINA.h>

void PrintMacAddress(byte mac[])
{
    for (int i = 5; i >= 0; i--)
    {
        if (mac[i] < 16)
        {
            Serial.print("0");
        }
        Serial.print(mac[i], HEX);
        if (i > 0)
        {
            Serial.print(":");
        }
    }
    Serial.println();
}

void PrintEncryptionType(int thisType)
{
    // read the encryption type and print out the name:
    switch (thisType)
    {
    case ENC_TYPE_WEP:
        Serial.print("WEP");
        break;
    case ENC_TYPE_TKIP:
        Serial.print("WPA");
        break;
    case ENC_TYPE_CCMP:
        Serial.print("WPA2");
        break;
    case ENC_TYPE_NONE:
        Serial.print("None");
        break;
    case ENC_TYPE_AUTO:
        Serial.print("Auto");
        break;
    case ENC_TYPE_UNKNOWN:
    default:
        Serial.print("Unknown");
        break;
    }
}

void Print2Digits(byte thisByte)
{
    if (thisByte < 0xF)
    {
        Serial.print("0");
    }
    Serial.print(thisByte, HEX);
}

void ListNetworks()
{
    // scan for nearby networks:
    Serial.println("** Scan Networks **");
    int numSsid = WiFi.scanNetworks();
    if (numSsid == -1)
    {
        Serial.println("Couldn't get a WiFi connection");
        while (true)
            ;
    }

    // print the list of networks seen:
    Serial.print("number of available networks: ");
    Serial.println(numSsid);

    // print the network number and name for each network found:
    for (int thisNet = 0; thisNet < numSsid; thisNet++)
    {
        Serial.print(thisNet + 1);
        Serial.print(") ");
        Serial.print("Signal: ");
        Serial.print(WiFi.RSSI(thisNet));
        Serial.print(" dBm");
        Serial.print("\tChannel: ");
        Serial.print(WiFi.channel(thisNet));
        byte bssid[6];
        Serial.print("\t\tBSSID: ");
        PrintMacAddress(WiFi.BSSID(thisNet, bssid));
        Serial.print("\tEncryption: ");
        PrintEncryptionType(WiFi.encryptionType(thisNet));
        Serial.print("\t\tSSID: ");
        Serial.println(WiFi.SSID(thisNet));
        Serial.flush();
    }
    Serial.println();
}

void PrintWifiStatus()
{
    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your board's IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();
    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
}

void ScanRefreshFunc()
{
    ListNetworks();
}

TickTimer::TickTimer(unsigned long delay, TimerType type)
{
    _delay = delay;
    _type = type;
    _lastTime = 0;
}
void TickTimer::Start(unsigned long t)
{
    _lastTime = t;
}
void TickTimer::Progress(unsigned long currTime)
{
    while (currTime > _lastTime && currTime - _lastTime >= _delay)
    {
        switch (_type)
        {
        case SCAN_REFRESH:
        {
            ScanRefreshFunc();
            break;
        }
        default:
        {
            Serial.println("default case");
            while (true)
                ;
        }
        }
        //_lastTime += _delay;
        _lastTime = currTime;
    }
}
