#include <Arduino.h>
#include <SPI.h>
#include <WiFiNINA.h>

#include "arduino_secrets.h"
#include "utils.h"

char g_ssid[] = SECRET_SSID;   // your WPA2 enterprise network SSID (name)
char g_user[] = SECRET_USER;   // your WPA2 enterprise g_username
char g_pass[] = SECRET_PASS;   // your WPA2 enterprise g_password
int g_status = WL_IDLE_STATUS; // the WiFi radio's g_status

char g_server[] = "www.google.com";
WiFiClient g_client;

unsigned long g_currentTime = 0;
TickTimer scanTimer(1 * 1000, SCAN_REFRESH);

void setup()
{
    //Initialize serial and wait for port to open:
    Serial.begin(9600);
    while (!Serial)
    {
        ; // wait for serial port to connect. Needed for native USB port only
    }

    // check for the WiFi module:
    if (WiFi.status() == WL_NO_MODULE)
    {
        Serial.println("Communication with WiFi module failed!");
        // don't continue
        while (true)
            ;
    }

    String fv = WiFi.firmwareVersion();
    if (fv < WIFI_FIRMWARE_LATEST_VERSION)
    {
        Serial.println("Please upgrade the firmware");
        while (true)
            ;
    }

    // print your MAC address:
    byte mac[6];
    WiFi.macAddress(mac);
    Serial.print("MAC: ");
    PrintMacAddress(mac);

    // scan for existing networks:
    Serial.println();
    Serial.println("Scanning available networks...");
    ListNetworks();

    // attempt to connect to WiFi network:
    // while (g_status != WL_CONNECTED)
    // {
    //     Serial.print("Attempting to connect to WPA SSID: ");
    //     Serial.println(g_ssid);
    //     // Connect to WPA2 enterprise network:
    //     // - You can optionally provide additional identity and CA cert (string) parameters if your network requires them:
    //     //      WiFi.beginEnterprise(g_ssid, g_user, g_pass, identity, caCert)
    //     g_status = WiFi.beginEnterprise(g_ssid, g_user, g_pass);
    //     // wait 10 seconds for connection:
    //     delay(10000);

    //     Serial.println("Connected to WiFi");
    //     PrintWifiStatus();
    // }

    // Serial.println("\nStarting connection to g_server...");
    // // if you get a connection, report back via serial:
    // if (g_client.connect(g_server, 80))
    // {
    //     Serial.println("connected to g_server");
    //     // Make a HTTP request:
    //     g_client.println("GET /search?q=arduino HTTP/1.1");
    //     g_client.println("Host: www.google.com");
    //     g_client.println("Connection: close");
    //     g_client.println();
    // }

    // g_currentTime = millis();
    // scanTimer.Start(g_currentTime);
}

void loop()
{
    // if there are incoming bytes available
    // from the g_server, read them and print them:
    while (g_client.available())
    {
        char c = g_client.read();
        Serial.write(c);
    }

    // if the g_server's disconnected, stop the g_client:
    if (!g_client.connected())
    {
        Serial.println();
        Serial.println("disconnecting from g_server.");
        g_client.stop();

        // do nothing forevermore:
        while (true)
            ;
    }
    // g_currentTime = millis();

    // scanTimer.Progress(g_currentTime);
}

int RSSIThreshold = 2; //get a good threshold value
int backoffBase = 0.1 * 1000;
int backoffLimit = 1 * 1000;

void backOffalgorithm(int otherRSSI, int otherSSID, int otherServerName)
{
    if (otherRSSI < RSSIThreshold)
        return;
    int backoff = random(backoffBase, backoffLimit);
    delay(backoff);
    //if other is active
    WiFi.begin(otherSSID);
    client.connect(otherServerName, 80);
    if (client.connected())
    {
        Serial.println("connected");
        // Make a HTTP request:
        client.println("GET /search?q=arduino HTTP/1.0");
        client.println();
    }
    client.stop();
    //else
    WiFi.beginAP(g_ssid);
    WiFiServer server(80);
    //if server.status == connected
    server.println("server answer");
}
