import math
from math import sqrt

import pynmea2


def get_distance(gpgga1: str, gpgga2: str, r: int = 6371e3):
    try:
        msg1 = pynmea2.parse(gpgga1)
        msg2 = pynmea2.parse(gpgga2)
    except pynmea2.ParseError as e:
        return (False, 0.0)
    phi1 = msg1.latitude * math.pi / 180.0
    phi2 = msg2.latitude * math.pi / 180.0
    delta_phi = (msg1.latitude - msg2.latitude) * math.pi / 180.0
    delta_lambda = (msg1.longitude - msg2.longitude) * math.pi / 180.0
    a = pow(math.sin(delta_phi / 2), 2) + math.cos(phi1) * \
        math.cos(phi2) * pow(math.sin(delta_lambda / 2), 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = r * c
    return (True, d)


def to_ddmmss(gpgga: str):
    msg = pynmea2.parse(gpgga)
    print(
        '%02d°%02d′%07.4f″' %
        (abs(
            msg.latitude),
            msg.latitude_minutes,
            msg.latitude_seconds),
        msg.lat_dir,
        end=', ')
    print(
        '%02d°%02d′%07.4f″' %
        (msg.longitude,
         msg.longitude_minutes,
         msg.longitude_seconds),
        msg.lon_dir)


if __name__ == '__main__':
    str1 = "$GPGGA,184353.07,1929.045,S,02410.506,E,1,04,2.6,100.00,M,-33.9,M,,0000*6D"
    str2 = "$GPGGA,181203.000,5742.4716,N,1156.3367,E,1,6,1.18,100.3,M,40.2,M,0,*5D"
    print("Position 1:", end=' ')
    to_ddmmss(str1)
    print("Position 2:", end=' ')
    to_ddmmss(str2)
    d = get_distance(str1, str2)
    print("Distance between Position 1 and Position 2:", d, "meters")
