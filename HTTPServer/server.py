from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs
from gps import get_distance

hostName = '192.168.43.177'  # myronfirst_wifi
# hostName = '192.168.0.101'  # LostAndFound
# hostName = 'localhost'
serverPort = 8080

# GET /get?key=myKey_otherKey&pos=my_GPS HTTP/1.1
# GET /update?key=key&pos=new_GPS HTTP/1.1

positions = {'0': '$GPGGA,184353.07,1929.045,S,02410.506,E,1,04,2.6,100.00,M,-33.9,M,,0000*6D',
             '1': '$GPGGA,181203.000,5742.4716,N,1156.3367,E,1,6,1.18,100.3,M,40.2,M,0,*5D',
             '2': '$GPGGA,184353.07,1929.045,S,02410.506,E,1,04,2.6,100.00,M,-33.9,M,,0000*6D',
             '3': '$GPGGA,181203.000,5742.4716,N,1156.3367,E,1,6,1.18,100.3,M,40.2,M,0,*5D'}
pairs = {'0_1': '2',
         '0_2': '3',
         '0_3': '1',
         '1_2': '3',
         '1_3': '2',
         '2_3': '0'}


def IsInteger(s):
    try:
        int(s)
    except BaseException:
        return False
    return True


def IsFloat(s):
    try:
        float(s)
    except BaseException:
        return False
    return True


class MyServer(BaseHTTPRequestHandler):

    def respond(self, msg):
        self.send_response(200, bytes(msg, 'utf-8'))
        self.end_headers()
        # self.wfile.write(bytes('<body>', 'utf-8'))
        # self.wfile.write(bytes(msg, 'utf-8'))
        # self.wfile.write(bytes('</body></html>', 'utf-8'))

    # def handle(self):
        # print("handle")
        # print(self.path)

    def do_HEAD(self):
        print("head")

    def do_GET(self):
        print("HEY")
        parsedPath = urlparse(self.path)
        mode = parsedPath.path
        query = parse_qs(parsedPath.query)
        msg = ''
        if mode == '/get':
            print("get")
            key = query['key'][0]
            myGPS = query['pos'][0]
            [keyStr0, keyStr1] = key.split('_')
            assert(IsInteger(keyStr0) and IsInteger(keyStr1))
            myKey = int(keyStr0)
            otherKey = int(keyStr1)

            # Get destination child token position
            parsedKey = '%d_%d' % (min(myKey, otherKey), max(myKey, otherKey))
            assert(parsedKey in pairs)
            destinationKey = pairs.get(parsedKey)
            assert(destinationKey in positions)
            otherGPS = positions.get(destinationKey)

            # Calculate distance from destination child token position
            [valid, distance] = get_distance(myGPS, otherGPS)

            msg = '%c%f' % (valid, distance)
            self.respond(msg)
        elif mode == '/update':
            print("update")
            key = query['key'][0]
            pos = query['pos'][0]
            assert(pos != 0)
            assert(key in positions)
            positions[key] = pos
        else:
            assert(False)


if __name__ == '__main__':
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print('Server started http://%s:%s' % (hostName, serverPort))
    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print('Server stopped.')
