# Campus Hide and Seek

*Campus Hide and Seek* is a game with tangible devices intended to be used by newly admitted exchange university students.

It is designed to promote fun and social interaction among students. It is similar to other Augmented Reality games like Geocaching.

It was conceptualized, designed, prototyped and playtested by a team of 4 exchange students as part of the CIU265 Interaction design project / DAT375/DIT460 Game Development Project, Chalmers University of Technology, Winter Semester 2021-2022.

The implementation is based on Arduino technology. The game is playable through the use of props, while the technical implementation is not in a finished state.

This repo holds the code-related side of the project.

The aim is to establish a client-server communication between then Arduino Nano 33 IoT and an HTTP Server, running in python. There is also code related to parsing GPS data.

- Load *Campus_Lost_Found/sketch* folder in Arduino IDE, upload and run *Campus_Lost_Found/sketch/sketch.ino* file.
- Run *HTTPServer/server.py* in python.

## Contributors

- Chenxu Guo
- Daniel Pettersson
- Myron Tsatsarakis
- Viktor Dimander
